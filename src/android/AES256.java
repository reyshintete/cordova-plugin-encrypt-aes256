package com.android.plugin;


import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.spec.IvParameterSpec;
import java.security.NoSuchAlgorithmException;
import javax.crypto.NoSuchPaddingException;

import android.util.Base64;
import android.util.Log;

import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import org.apache.cordova.PluginResult.Status;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;




public class AES256 extends CordovaPlugin{

    private final static String alg = "AES";
    //Modo de operacion y esquema de relleno
    private final static String cI = "AES/CBC/PKCS5Padding";
    private final static String key="kwnsjakASJhjhkhJVasAs152kjas1sas";
    private final static String iv="0123456789ABCDEF";

    
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("Encrypt")) {
            try{
                callbackContext.success(Encrypt(args.getString(0)));
            }catch (Exception e) {
                return false;
            }
            return true;
        }else if(action.equals("Decrypt")){
            try{
                callbackContext.success(Decrypt(args.getString(0)));
            }catch (Exception e) {
                return false;
            }
            return true;
        } else{
            return false;    
        }
        
    }

    public static String Encrypt(String Text) throws Exception {
        Cipher cipher = Cipher.getInstance(cI);
        SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), alg);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(iv.getBytes());
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivParameterSpec);
        byte[] encrypted = cipher.doFinal(Text.getBytes());
        return Base64.encodeToString(encrypted, Base64.DEFAULT);
    }


    public static String Decrypt( String Text) throws Exception {
        Cipher cipher = Cipher.getInstance(cI);
        SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), alg);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(iv.getBytes());
        byte[] enc = Base64.decode(Text.getBytes(),Base64.DEFAULT);
        cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivParameterSpec);
        byte[] decrypted = cipher.doFinal(enc);
        return new String(decrypted);
    }
}