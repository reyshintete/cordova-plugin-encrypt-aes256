#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonCryptor.h>
#import <Cordova/CDVPlugin.h>


@interface CordovaPluginEncrypt : CDVPlugin{
}

-  (void)Encrypt:(CDVInvokedUrlCommand *)command;
-  (void)Decrypt:(CDVInvokedUrlCommand *)command;

@end	