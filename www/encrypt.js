module.exports = {
    Encrypt: function (Password, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "CordovaPluginEncrypt", "Encrypt", [Password]);
    },
    Decrypt: function (Password, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "CordovaPluginEncrypt", "Decrypt", [Password]);
    }
};
